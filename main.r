source('cliquecode.R')



### gm.restart
### nstart :: Int
### prob   :: Double
### seed   :: Int
### data   :: Matrix
### forward :: Bool
### backward :: Bool
### score :: String
### out :: List containing $models, $score, $call and $trace
### This functions runs the gm.search nstart times. Each times it calls it with 
### a new random adjacency matrix, where each pair of vertexes has probability
### prob to be connected.
gm.restart <- function(nstart, prob, seed=F, data, forward=T, backward=T, score="bic"){
  if(seed){
    set.seed(seed)
  }
  
  bestScore <- Inf
  bestRes <- c()
  
  n = ncol(data)
  for(i in 1:nstart){
    g <- randomGraph(n, prob)
    res <- gm.search(data, g, forward, backward, score)
    if(res[["score"]] < bestScore){
      bestScore <- res[["score"]]
      bestRes <- res
      print(bestRes)
    }
  }
  res
}

### gm.seach
### data :: Matrix
### graph.init   :: Matrix
### forward :: Bool
### backward :: Bool
### score :: String
### out :: List containing $models, $score, $call and $trace
### This functions uses greedy hill climbing to search for best graphical 
### model. At each iteration, the neighbours it considers are those models
### that can be created by either adding or removing an edge.
gm.search <- function(data, graph.init, forward=T, backward=T, score="bic"){
  n = nrow(graph.init)
  m = nrow(data)
  
  trace = list()
  
  ## Calculate degrees of freedom for empty graph
  dfConst = loglin(table(data), list(), print=F)[["df"]] + 1
  
  bestScore <- Inf
  bestGraph <- graph.init
  
  didImprove <- T
  
  while(didImprove){
    didImprove <- F
    
    tmp <- bestGraph
    
    tr <- ""
    
    for(i in 1:n){
      for(y in 1:i){
        if(i == y) next
        
        
        c = matrix(bestGraph, n, n)
        if(c[i,y] == 1 && backward){
          c[i,y] <- 0
          c[y,i] <- 0
          res <- loglin(table(data), margins(c), print=F)
          s <- res[["lrt"]] + scoreF(m, dfConst - res[["df"]], score)
          if(s < bestScore){
            tmp <- c
            bestScore <- s
            didImprove <- T
            tr <- capture.output(cat("Removed an edge from", y, " - ", i, " (score= ", s, ")"))
          }
        }
        
        c = matrix(bestGraph, n, n)
        if(c[i,y] == 0 && forward){
          c[i,y] <- 1
          c[y,i] <- 1
          res <- loglin(table(data), margins(c), print=F)
          s <- res[["lrt"]] + scoreF(m, dfConst- res[["df"]], score)
          if(s < bestScore){
            tmp <- c
            bestScore <- s
            didImprove <- T
            tr <- capture.output(cat("Added an edge from", y, " - ", i, " (score= ", s, ")"))
          }
        }
        
      }
    }
    
    bestGraph <- tmp
    if(didImprove){
      trace <- append(trace, tr)
    }
  }
  
  
  list(score=bestScore, models=margins(bestGraph), call=match.call(), trace=trace, graph=bestGraph)
}

## randomGraph
## dim :: Int
## prob :: Float
## out  :: Matrix
## This method creates a square matrix of dimensions dim * dim.
## Each value is 1 with probability prob and 0 with probability (1-prob)
randomGraph <-function(dim, prob){
  m <- matrix(0,dim,dim)
  rv <- runif(((dim*(dim-1))/2), .0, 1.0) 
  
  for(i in 1:dim){
    for(j in 1:i){
      if(i == j) next
      if(runif(1,0,1) <= prob){
        m[i,j] <- 1
        m[j,i] <- 1
      }
    }
  }
  return (m)
}

## Margins
## graph :: Matrix
## out   :: List
## Outputs the cliques of a graph in a format suitable for loglin.
margins <- function(graph){
  post.process(find.cliques(c(), 1:nrow(graph), c(), graph, list()))
}


## scoreF
## n :: Int
## df :: Int
## score :: String
## out :: Float
## Calculates the complexity cost of the model.
scoreF <- function(n, df, score){
  if(score == "bic"){
    log(n) * df
  }
  else{
    2 * df
  }
}


## drawGraph
## model :: List returned by gm.search
## Helper function that creates an image of the final graphical model.
drawGraph <- function(model){
  library(igraph)
  g <- graph.adjacency(model[["graph"]], mode="undirected")
  plot(g)
}




#data <- read.table("coronary.txt", header=T)
data <- read.csv('rhc-small.txt')

